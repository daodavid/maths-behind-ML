<h1 face = "Times New Roma" color='#3f134f'> Machine Learning </h1>   

<h2>Maths</h2>
   <ul size="2">
             <li><a  href='#reg'>Mathematical Analysis - in Progress</a> </li>    
             <li><a  href='/notebooks/Math/Probability and Statistic/'>Probability and Statistic - in Progress</a> </li>
   </ul>  

<h2>ML</h2>
  <font   face = "Times New Roma" color='#3f134f' > From maths to implementations of:</font>
  <br>
  <font   face = "Times New Roma" color='#3f134f' > 
    <ul size="2" style="margin-left: 30px">
      <li><a href='/notebooks/ML/Linear%20Regression'>Linear Regression - in Progress</a></li>
      <li><a href='/notebooks/ML/Logistic%20Regression'>Logistic Regression - - in Progress </a></li>
      <li><a href='/notebooks/ML/Softmax%20Regression'>Softmax regression</a></li>
   </ul> 
 </font>

<h2>Projects</h2>
